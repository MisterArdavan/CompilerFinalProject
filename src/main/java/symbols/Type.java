package symbols;

import lexer.Tag;
import lexer.Word;

public class Type extends Word {
    public int width = 0; // width is used for storage allocation

    public Type(String s, int tag, int w) {
        super(s, tag);
        width = w;
    }

    public static final Type
            Char = new Type("char", Tag.BASIC, 4),
            Int = new Type("int", Tag.BASIC, 4),
            Void = new Type("void", Tag.BASIC, 0);

    public static boolean numeric(Type p) {
        if (p == Type.Char || p == Type.Int)
            return true;
        else
            return false;
    }

    public static Type max(Type p1, Type p2) {
        if (!numeric(p1) || !numeric(p2))
            return null;
        else if (p1 == Type.Int || p2 == Type.Int)
            return Type.Int;
        return Type.Char;
    }
}
