package parser;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ParseTable {
    private static final int NT_START_INDEX = 24;
    public ArrayList<String> symbols;
    private ParseTableEntry[][] table;
    private String productionLHS[];
    private int productionSize[];
    private HashMap<String, ArrayList<String>> followMap;

    public ParseTable() {
        symbols = new ArrayList<String>();
        table = new ParseTableEntry[96][52];
        productionLHS = new String[56];
        productionSize = new int[56];
        followMap = new HashMap<String, ArrayList<String>>();
        make();
    }

    public ParseTableEntry getEntry(int stateNumber, String symbol) {
        return table[stateNumber][symbols.indexOf(symbol)];
    }

    public Integer getFirstNTWithGoTo(int stateNumber) {
        for (int i = NT_START_INDEX; i < symbols.size(); i++) {
            if (table[stateNumber][i].getAction() == Action.GOTO)
                return i;
        }
        return null;
    }

    public String getSymbol(int index) {
        return symbols.get(index);
    }

    public int getProductionSize(int index) {
        return productionSize[index];
    }

    public String getProductionLHS(int index) {
        return productionLHS[index];
    }

    public HashMap<String, ArrayList<String>> getFollowMap() { return followMap; }

    private void make() {
        File file = new File("./parseTable.csv");
        Scanner scanner;
        try {
            scanner = new Scanner(file);
            String symbolsLine = scanner.nextLine();
            StringTokenizer tokenizer = new StringTokenizer(symbolsLine);
            while (tokenizer.hasMoreTokens()) {
                symbols.add(tokenizer.nextToken());
            }
            for (int row = 0; row < 96; row++) {
                StringTokenizer st = new StringTokenizer(scanner.nextLine());
                if (row != Integer.parseInt(st.nextToken()))
                    System.err.println("something is wrong");
                for (int col = 0; col < 52; col++) {
                    String word = st.nextToken();
                    if (word.equals(",")) {
                        table[row][col] = new ParseTableEntry(0, Action.ERROR);
                    } else if (word.matches(",s(.*)")) {
                        int value = Integer.parseInt(word.substring(2));
                        table[row][col] = new ParseTableEntry(value, Action.SHIFT);
                    } else if (word.matches(",r(.*)")) {
                        int value = Integer.parseInt(word.substring(2));
                        table[row][col] = new ParseTableEntry(value, Action.REDUCE);
                    } else if (word.matches(",(.*)")) {
                        int value = Integer.parseInt(word.substring(1));
                        table[row][col] = new ParseTableEntry(value, Action.GOTO);
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        file = new File("./productionRules.txt");
        try {
            scanner = new Scanner(file);
            for (int row = 0; row < 56; row++) {
                if (scanner.nextInt() != row)
                    System.err.println("Error in productionRules");
                productionSize[row] = scanner.nextInt();
                productionLHS[row] = scanner.next();
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        file = new File("./followTable.txt");
        try {
            scanner = new Scanner(file);
            for (int row = 0; row < 28; row++) {
                String line = scanner.nextLine();
                StringTokenizer stringTokenizer = new StringTokenizer(line);
                ArrayList<String> follow = new ArrayList<String>();
                String NT = stringTokenizer.nextToken();
                while (stringTokenizer.hasMoreElements()) {
                    follow.add(stringTokenizer.nextToken());
                }
                followMap.put(NT, follow);
            }
            scanner.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
