package parser;

public class ParseTableEntry {
    private int value;
    private Action action;

    public int getValue() {
        return value;
    }

    public Action getAction() {
        return action;
    }
    public ParseTableEntry (int v, Action a){
        value = v;
        action = a;
    }

    @Override
    public String toString() {
        return ""+action+value;
    }
}
