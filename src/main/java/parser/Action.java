package parser;

public enum Action {
    GOTO,
    REDUCE,
    SHIFT,
    ERROR
}
