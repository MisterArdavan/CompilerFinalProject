package main;

import lexer.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
//import parser.*;


public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        FileInputStream is = new FileInputStream(new File(args[0]));
        System.setIn(is);
        Lexer lex = new Lexer();
        for (int i = 0; i<33; i++){
            try {
                System.out.println(lex.scan().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
