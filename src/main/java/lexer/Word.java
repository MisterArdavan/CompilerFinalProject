package lexer;

public class Word extends Token {
    public String lexeme = "";
    public Word(String s, int tag) {
        super(tag);
        lexeme = s;
    }
    public String toString() {
        return lexeme;
    }
    public static final Word
            eq = new Word( "==", Tag.EQ),
            minus = new Word( "minus", Tag.MINUS),
            plus = new Word ("plus", Tag.PLUS),
            temp = new Word( "t", Tag.TEMP);

}
