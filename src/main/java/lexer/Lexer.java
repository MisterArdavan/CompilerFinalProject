package lexer;

import symbols.Type;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

public class Lexer {
    public static int line = 1;
    private char peek = ' ';
    private Hashtable words = new Hashtable(); // Symbol Table of the grammar
    private Set<String> beforeUnary = new HashSet<String>(Arrays.asList("{", "-", "+", "*", "(", "<", "==", "=", "return", ";"));
    private String prev = ";";

    private void reserve(Word w) {
        words.put(w.lexeme, w);
    }

    public Lexer() {
        reserve(new Word("if", Tag.IF));
        reserve(new Word("else", Tag.ELSE));
        reserve(new Word("while", Tag.WHILE));
        reserve(new Word("return", Tag.RETURN));
        reserve(Type.Void); //TODO reserve void in this way?
        reserve(Type.Int);
    }

    void readch() throws IOException {
        peek = (char) System.in.read();
    }

    boolean readch(char c) throws IOException {
        readch();
        if (peek != c)
            return false;
        peek = ' ';
        return true;
    }

    boolean isUnary(char peek) {
        if (peek == '+' || peek == '-') {
            if (beforeUnary.contains(prev))
                return true;
            return false;
        }
        return false;
    }

    public Token scan() throws IOException {
        for (; ; readch()) {
            if (peek == ' ' || peek == '\t')
                continue;
            else if (peek == '\r' || peek == '\n')
                line = line + 1;
            else
                break;
        }

        switch (peek) {
            case '=':
                if (readch('=')) {
                    prev = "==";
                    return Word.eq;
                } else {
                    prev = "=";
                    return new Token('=');
                }
        }

        if (Character.isDigit(peek) || isUnary(peek)) {
            int sign = 1;
            if (isUnary(peek)) {
                if(peek == '-') sign = -1;
                readch();
            }
            //todo error handling
            int v = 0;
            do {
                v = 10 * v + Character.digit(peek, 10);
                readch();
            } while (Character.isDigit(peek));
            prev = "number";
            v *= sign;
            return new Num(v);
        }

        if (Character.isLetter(peek)) {
            StringBuffer b = new StringBuffer();
            do {
                b.append(peek);
                readch();
            } while (Character.isLetterOrDigit(peek));
            String s = b.toString();
            Word w = (Word) words.get(s);
            if (w != null) {
                prev = w.lexeme;
                return w; // return if it's already in Symbol Table
            }
            w = new Word(s, Tag.ID);
            words.put(s, w);
            prev = w.lexeme;
            return w;
        }

        Token token = new Token(peek);
        prev = "" + peek;
        peek = ' ';
        return token;
    }
}
