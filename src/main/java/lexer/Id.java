package lexer;

import symbols.Type;

public class Id {
    private Word id;
    private Type type;

    public Id(Word id, Type t){
        this.id = id;
        type = t;
    }

}
